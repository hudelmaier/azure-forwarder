const axios = require("axios");

const {IOT20K_API_KEY} = process.env;

module.exports = async function(context, messages) {

    for (const message of messages) {
        context.log(`Processed message: ${JSON.stringify(message, null, 2)}`);
        if (!message.records) {
            continue;
        }
        for (const record of message.records) {

            if (record.operationName !== 'deviceConnect') {
                continue;
            }

            const properties = JSON.parse(record.properties);
            if (!properties.maskedIpAddress || !properties.deviceId) {
                continue;
            }

            // TODO: As soon as available on data.iot20k.com, batch all messages together in a single request
            // TODO: As soon as available on data.iot20k.com, send the maskedIpAddress without replacing the XXXs here
            context.log(`deviceId: ${properties.deviceId}, maskedIpAddress: ${properties.maskedIpAddress}`);
            await axios.post("https://data.iot20k.com", {
                id: properties.deviceId,
                ip: properties.maskedIpAddress.replace('XXX', '1')
            }, {
                headers: {
                    'API-KEY': IOT20K_API_KEY
                }
            });
        }
    }
}
