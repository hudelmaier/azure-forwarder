# Azure IoT 20k Forwarder

[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgl.githack.com%2Fhudelmaier%2Fazure-forwarder%2F-%2Fraw%2Fmaster%2Farm-template.json)

Note: Using githack.com as a workaroud until https://gitlab.com/gitlab-org/gitlab/-/issues/16732 is fixed or project is moved to GitHub.

# How does it work?

This illustrates the way the forwarder works:

![Diagram](diagram.png)

# Setup

* In your IoT Hub, go to `Built-in endpoints`.
  * Add a new consumer group `iot20k`. 
  * Copy the `Event Hub-compatible endpoint` with a `Shared Access Policy` of `service`.
* Click on the `Deploy to Azure` button above. In the `Custom Deployment` form that opens:
  * Create a new resource group, e.g. `iot20k-forwarder`.
  * Add your IoT 20k API Key (you can get it from [here](https://iot20k.com/profile)).
  * Paste the `Event Hub-compatible endpoint` that you copied earlier.
  * Click on `Review + Create` and then on `Create`
* After the deployment is complete, go to `Diagnostic settings` in your IoT Hub. Add a new diagnostic setting:
  * Check the `Connections` checkbox
  * Check the `Stream to an event hub` checkbox, select the Event hub namespace `iot20k-connections-event-hubs-xyz` 
    and the Event hub name `iot20k-connection-event-hub`. You must select the `RootManagedSharedAccessKey` as 
    the Event hub policy name (see FAQ).
* Go to <https://iot20k.com/dashboard>. As soon as data is received by your IoT Hub, communication events should
  be visible here. Please a allow for up to 10 minutes until the settings in IoT Hub have taken effect.

# Perform the deployment using the command line

Alternatively to using the `Deploy to Azure` button, you can also use the `az` CLI:

```
az group create iot20k-forwarder --location LOCATION
az deployment group create \
    --resource-group iot20k-forwarder \
    --template-file arm-template.json \
    --parameters \
    apiKey=YOUR_IOT20K_API_KEY \
    iotHubConnectionString='Endpoint=sb://ihsuprodamres051dednamespace.servicebus.windows.net/;SharedAccessKeyName=service;SharedAccessKey=xxx;EntityPath=iothub-ehub-xxx-6418180-d16ea6c1ab'
```

# FAQ

* When deploying you get the message "resource provider 'microsoft.insights' not registered for the subscription" -> Register Resource Provider 'microsoft.insights' in the Azure Portal: Subcriptions > *Your subscription* > Resource Providers

* Why are you using the `RootManageSharedAccessKey` when configuring the diagnostics? -> Diagnostics require the used shared access key to have the 'Manage' right.
