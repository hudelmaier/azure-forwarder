const axios = require("axios");

const {IOT20K_API_KEY} = process.env;

module.exports = async function(context, messages) {

    for (let i = 0; i < messages.length; ++i) {
        const message = messages[i];
        const deviceId = context.bindingData.systemPropertiesArray[i]['iothub-connection-device-id'];
        const bytes = message.length;
        context.log(`Processed message from device ${deviceId} with ${bytes} bytes`);

        // TODO: As soon as available on data.iot20k.com, batch all messages together in a single request
        await axios.post("https://data.iot20k.com", {
            id: deviceId,
            bytes
        }, {
            headers: {
                'API-KEY': IOT20K_API_KEY
            }
        });
    }

    context.done();
};
