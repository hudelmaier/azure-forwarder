# Notes

`eventHubName` must be set to an empty string, as there is otherwise an error when 
starting the Azure function ("Can't figure out which ctor to call").  The name of the 
Event Hub is included in the connection string when the connection string is copied from 
the IoT Hub UI.
